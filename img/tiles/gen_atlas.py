#!/usr/bin/python

import Image
import math
import os
import re
import sys

cnt = 0
tileSize = 32
tilesDir = "./tiles"+str( tileSize )
edge = tilesDir+"/edge"+str( tileSize )+".png"
tiles = []

dirList = os.listdir(tilesDir)

# Load tiles paths, count number of tiles
for tileDir in dirList:
  if tileDir[-4:] == ".png": 
    tiles.append( tilesDir + "/" + tileDir )
    try:
      x = int( re.findall( "_\d_", tileDir )[0][1] )
      if( x!=1 and x!=2 and x!=4 ):
        print >> sys.stderr, "Wrong _X_ value in: '" + tileDir + "'.\nX should equals 1, 2 or 4\n" 
        sys.exit( 1 )
      cnt = cnt + x
    except IndexError:
      print >> sys.stderr, "Non-standard file name: '"+tileDir+"'.\nCorrect format is: Tn_X_name.png"
      sys.exit( 1 )
tiles.sort()

# Find fitting square for texture atlas
x = math.ceil( math.sqrt( cnt ) )

outSize = int( x*tileSize )
out = Image.new( 'RGBA', ( outSize, outSize ), ( 255, 255, 255, 0 ) )

i = 0
for imDir in tiles:
  for j in range( 0, int ( re.findall( "_\d_", imDir )[0][1] ) ):
    bg = Image.open( imDir )
    bg = bg.rotate( 90*j )
    if( i!=0 ):
      # Add 'edge.png' as foreground
      fg = Image.open( "./edge" + str( tileSize ) + ".png" )
      out.paste( Image.alpha_composite( bg, fg ), ( int( i%x*32 ), int( math.floor( i/x )*32 ) ) )
    else:
      # Paste '00_' images without added 'edge.png'
      out.paste( bg, ( int( i%x*32 ), int( math.floor( i/x )*32 ) ) )
    i = i+1

out.save( "tile_atlas"+str( tileSize )+".png" )
