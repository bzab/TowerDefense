#!/usr/bin/python

import Image
import math
import os
import re
import sys

cnt = 0
tileSize = 32
tilesDir = "./enemies"+str( tileSize )
tiles = []

dirList = os.listdir(tilesDir)

# Load tiles paths, count number of tiles
for tileDir in dirList:
  if tileDir[-4:] == ".png": 
    tiles.append( tilesDir + "/" + tileDir )
    if re.search( "(?<=_)\d+\.", tileDir ):
      cnt = cnt + 1
    else:
      print >> sys.stderr, "Non-standard file name: '"+tileDir+"'.\nCorrect format is: name_X.png"
      sys.exit( 1 )
tiles.sort()

# Find fitting square for texture atlas
x = math.ceil( math.sqrt( cnt ) )

outSize = int( x*tileSize )
out = Image.new( 'RGBA', ( outSize, outSize ), ( 255, 255, 255, 0 ) )

i=0
for imDir in tiles:   
    bg = Image.open( imDir )
    out.paste( bg, ( int( i%x*32 ), int( math.floor( i/x )*32 ) ) )
    i = i+1

out.save( "enemy_atlas"+str( tileSize )+".png" )
