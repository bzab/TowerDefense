#pragma once

#include "button.h"
#include "play_state.h"

class PlayButton: public Button {
public:
	using Button::Button;

	void onClick();
};
