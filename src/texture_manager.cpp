#include "texture_manager.h"

#include <iostream>

TextureManager::TextureManager(SDL_Renderer* r) {
	renderer = r;
}

Texture* TextureManager::get(const std::string &path) {

	// Check if texture isnt already in dict -- if Y, ret pointer
	m_iter = m_textures.find(path);
	if (m_iter != m_textures.end()) {
		return m_iter->second;
	}
	// Else load it
	return loadFromFile(path);

}

Texture* TextureManager::loadFromFile(const std::string &path) {
	Texture* loaded_texture = new Texture(path, renderer);
	m_textures[path] = loaded_texture;
	return loaded_texture;
}

void TextureManager::purge() {
	for (m_iter = m_textures.begin(); m_iter != m_textures.end(); m_iter++) {
		delete m_iter->second;
	}
	m_textures.clear();
}

TextureManager::~TextureManager() {
	// Delete textures from map
	purge();
}
