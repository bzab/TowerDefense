#include "edit_state_map.h"

EditStateMap::EditStateMap(int x, int y, int tile_size, const std::string &path,
		const std::string &atlas_path, Game* game) :
		Map(x, y, tile_size, path, atlas_path, game) {
	m_idTileSwap = 0;
	m_idTurret = 0;
	m_spawn_sprite = game->texture_manager->get(atlas_path);
	//                               x,      y,     w,         h
	m_spawn_clip = new SDL_Rect { tile_size, 0, tile_size, tile_size };
}

void EditStateMap::handleEvent(SDL_Event* e) {
	for (auto &row : m_tiles) {
		for (auto &tile : row) {
			tile->handleEvent(e);
			if (tile->isActive() && m_idTileSwap) {
				swapTile(m_idTileSwap);
				m_idTileSwap = 0;
			}
		}
	}
}

void EditStateMap::render() {
	Map::render();

	for (auto &spawn : m_spawns) {
		m_spawn_sprite->render(spawn->x, spawn->y, m_spawn_clip);
	}
}

void EditStateMap::saveMap(const std::string &path) {
	// Open the map and clear the file
	std::ofstream map_file(path, std::ofstream::out | std::ofstream::trunc);

	// If the map couldn't be loaded
	if (!map_file.is_open()) {
		std::cerr << "Unable to load map file:\n" << path << std::endl;
		m_game->quit();
	} else {
		map_file << m_tiles[0].size() << " " << m_tiles.size() << "\n";
		for (auto &row : m_tiles) {
			for (auto &tile : row) {
				map_file << tile->getTileId() << " ";
			}
			map_file << "\n";
		}
		map_file << m_spawns.size() << "\n";
		for (auto &spawn : m_spawns) {
			map_file << spawn->x << " " << spawn->y << "\n";
		}
		map_file << std::flush;

		map_file.close();
	}
}

void EditStateMap::setSwap(int tile_id) {
	m_idTileSwap = tile_id;
}

void EditStateMap::swapTile(int tile_id) {
	if (tile_id != 1) {  // If selected is not spawn marker
		for (auto &row : m_tiles) {
			for (auto &tile : row) {
				if (tile->isActive()) {
					SDL_Point position = tile->getPosition();
					int size = tile->getClip().w; // All tiles are squares, so h = w
					delete tile;
					tile = new Tile(position.x, position.y, tile_id, size,
							m_atlas_path, m_game);
				}
			}
		}
	} else {
		// Put spawn marker
		for (auto &row : m_tiles) {
			for (auto &tile : row) {
				if (tile->isActive()) {
					int x = tile->getPosition().x;
					int y = tile->getPosition().y;
					bool is_present = false;
					for (unsigned int i = 0; i < m_spawns.size(); ++i) {
						if (x == m_spawns[i]->x && y == m_spawns[i]->y) {
							is_present = true;
							delete m_spawns[i];
							m_spawns.erase(m_spawns.begin() + i);
							break;
						}
					}
					if (!is_present) {
						m_spawns.push_back(new SDL_Point {
								tile->getPosition().x, tile->getPosition().y });
					}

				}
			}
		}
	}
}

EditStateMap::~EditStateMap() {
	delete m_spawn_clip;
	m_spawn_clip = nullptr;
}
