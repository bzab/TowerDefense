#pragma once

#include <vector>

#include "game_state.h"

#include "button.h"
#include "load_map_button.h"
#include "edit_state_map.h"
#include "menu_button.h"
#include "save_map_button.h"
#include "swap_button.h"
#include "tile_selection_bar.h"

class EditState: public GameState {
public:
	EditState(Game* game);
	EditState(const EditState &) = delete;
	EditState& operator=(const EditState &) = delete;

	void pause();
	void resume();
	void draw();
	void update(const double dt);
	void handleInput();

	~EditState();

private:
	EditStateMap* m_map;
	Texture* m_img;
	TileSelectionBar* m_tile_bar;
	std::vector<Button*> m_buttons;
};
