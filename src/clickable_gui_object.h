#pragma once

#include "game.h"
#include "gui_object.h"

class ClickableGUIObject: public GUIObject {
public:
	bool isClicked();

	void handleEvent(SDL_Event* e);
	virtual void onHover();
	virtual void offHover();
	virtual void onClick() {
	}
	;
	virtual void render() = 0;

	virtual ~ClickableGUIObject() {
	}
	;

protected:
	bool m_is_clicked;
	bool m_is_hovered;

	virtual int getWidth();
	virtual int getHeight();

	Game* m_game;
};
