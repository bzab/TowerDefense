#pragma once

#include <list>

#include "bullet.h"
#include "enemy.h"
#include "geo_2d.h"
#include "map.h"
#include "tower_selection_bar.h"

class Enemy;

class PlayStateMap: public Map {
public:
	PlayStateMap(int x, int y, int tile_size, const std::string &path,
			const std::string &atlas_path, Game* game, TowerSelectionBar* tsb);
	PlayStateMap(const PlayStateMap &) = delete;
	PlayStateMap& operator=(const PlayStateMap &) = delete;

	SDL_Point getPosition();

	uint8_t getAvailableDirections(unsigned int x, unsigned int y);

	int getVectorSizeX();
	int getVectorSizeY();

	void handleEvent(SDL_Event* e);
	void render();
	void spawnBullet(double x, double y, double angle, double range, double spd,
			int dmg);
	void update(const double dt);

	~PlayStateMap();
private:
	double m_spawn_timer;

	std::list<Bullet*> m_bullets;
	std::list<Bullet*>::iterator m_iter_b;
	std::list<Enemy*> m_enemies;
	std::list<Enemy*>::iterator m_iter_e;
	TowerSelectionBar* m_tower_bar;

	std::vector<std::vector<uint8_t> > m_roads;

	void spawnEnemy(int x, int y, int index_in_atlas, int tex_size,
			int textures_num, double v, int hp, int hb_x, int hb_y, int hb_w,
			int hb_h);
	void removeDeadBullets();
	void removeDeadEnemies();
	void resetSpawnTimer();
};
