#include "button.h"

Button::Button(int x, int y, const std::string &path, Game* game) {
	m_game = game;
	m_is_hovered = false;
	m_position.x = x;
	m_position.y = y;
	m_texture_manager = m_game->texture_manager;
	m_sprite = m_texture_manager->get(path);

	m_clip = new SDL_Rect;
	m_clip->x = 0;
	m_clip->y = 0;
	m_clip->h = m_sprite->getHeight();
	m_clip->w = m_sprite->getWidth();
}

void Button::onClick() {
}

void Button::render() {
	// Make active button darker
	if (m_is_hovered) {
		m_sprite->setColor(200, 200, 200);
	} else {
		m_sprite->setColor(255, 255, 255);
	}

	m_sprite->render(m_position.x, m_position.y, m_clip);
}

Button::~Button() {
	delete m_clip;
}
