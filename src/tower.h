#pragma once

#include <vector>

#include "clickable_gui_object.h"
#include "geo_2d.h"
#include "texture_manager.h"

class PlayStateMap;

class Tower: public ClickableGUIObject {
public:
	Tower(int x, int y, const std::string &path, TextureManager* tm,
			int index_in_atlas, int tex_size, int b_t_num, int textures_num,
			int dmg, double range, double bul_spd, double rof);
	Tower(int x, int y, const Tower &t);
	Tower(const Tower &) = delete;
	Tower& operator=(const Tower &) = delete;

	bool isActive();

	void flipActive();
	void setActive(bool);
	void onClick();

	int getTowerId();

	double getRange();

	void render();
	void rotate(double angle);
	void updateTimer(const double dt);

	bool update(const double dt, Vec2D<double> &aim, Vec2D<double> target_vel,
			PlayStateMap* map);

	~Tower();
private:
	std::vector<SDL_Rect*> m_clips;

	bool m_is_active;

	double m_angle;
	double m_cooldown_timer;
	double m_bullet_spd; // Pixels / s
	double m_range; // Pixels
	double m_rof; // Time between shots

	unsigned int m_base_textures_num;
	int m_dmg;
	int m_id;

	int getHeight();
	int getWidth();
};
