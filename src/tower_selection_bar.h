#pragma once

#include <vector>

#include "remove_tower_icon.h"
#include "selection_bar.h"
#include "tower.h"

class TowerSelectionBar: public SelectionBar {
public:
	TowerSelectionBar(int x, int y, const std::string &bar_path,
			const std::string &towers_path, Game* game, int tile_size);
	TowerSelectionBar(const TowerSelectionBar &) = delete;
	TowerSelectionBar& operator=(const TowerSelectionBar &) = delete;

	Tower* getNewTower(SDL_Point pos);
	Tower* getSelectedTower();

	bool isRemove();

	void handleEvent(SDL_Event* e);
	void render();

	~TowerSelectionBar();

private:
	std::vector<Tower*> m_towers;

	RemoveTowerIcon* m_remove_tower_icon;

	bool m_remove;
};
