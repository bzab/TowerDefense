#pragma once

#include <string>

#include "clickable_gui_object.h"
#include "game.h"
#include "tower.h"

class Tile: public ClickableGUIObject {
public:
	Tile(int x, int y, int n, int size, const std::string &path, Game* game);
	Tile(const Tile &) = delete;
	Tile& operator=(const Tile &) = delete;

	bool isActive();
	bool isSpawn();
	bool hasTower();

	int getTileId();

	void flipActive();
	virtual void onClick();
	void removeTower();
	void render();
	void setActive(bool f);
	void setTower(const Tower &tower);

	Tower* getTower();

	~Tile();
private:
	Tower* m_tower;

	bool m_is_active;

	int m_id;
};
