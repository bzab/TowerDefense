#pragma once

#include "button.h"
#include "edit_state.h"

class EditButton: public Button {
public:
	using Button::Button;

	void onClick();
};
