#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "game.h"
#include "tile.h"
#include "sdl_includes.h"

class Map {
public:
	Map(int x, int y, int tile_size, const std::string &path,
			const std::string &atlas_path, Game* game);
	Map(const Map &) = delete;
	Map& operator=(const Map &) = delete;

	std::string& getAtlasPath();
	std::string& getMapPath();

	void clearMap();
	virtual void handleEvent(SDL_Event* e) = 0;
	void loadMap(int tile_size, const std::string &path);
	virtual void render();
	virtual void saveMap(const std::string &path) {
	}
	;
	virtual void setSwap(int tile_id) {
	}
	;
	virtual void swapTile(int tile_id) {
	}
	;
	void update(double dt) {
	}
	;

	virtual ~Map();
protected:
	std::string m_atlas_path;
	std::string m_file_path;
	std::vector<std::vector<Tile*> > m_tiles;
	std::vector<SDL_Point*> m_spawns;
	SDL_Point m_position;

	Game* m_game;
};
