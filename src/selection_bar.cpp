#include "selection_bar.h"

SelectionBar::SelectionBar(int x, int y, const std::string &path, Game* game) {
	m_position.x = x;
	m_position.y = y;
	m_texture_manager = game->texture_manager;
	m_sprite = m_texture_manager->get(path);

	m_clip = new SDL_Rect;
	m_clip->x = 0;
	m_clip->y = 0;
	m_clip->h = m_sprite->getHeight();
	m_clip->w = m_sprite->getWidth();

}

void SelectionBar::render() {
	m_sprite->render(m_position.x, m_position.y, m_clip);
}

SelectionBar::~SelectionBar() {
	delete m_clip;
}
