#include "bullet.h"

#include <cmath>

Bullet::Bullet(double x, double y, const std::string &path, TextureManager* tm,
		double angle, double range, double spd, int dmg) {
	m_angle = angle;
	m_clip = new SDL_Rect { 0, 0, 3, 2 };
	m_dmg = dmg;
	m_is_alive = true;
	m_position.x = x;
	m_position.y = y;
	m_range = range;
	m_sprite = tm->get(path);
	m_texture_manager = tm;
	m_velocity = {x = sin( angle * M_PI / 180.0 )*spd, y = -cos( angle * M_PI / 180.0 )*spd};
}

int Bullet::getDmg() {
	return m_dmg;
}

void Bullet::destroy() {
	m_is_alive = false;
}

void Bullet::render() {
	m_sprite->render(static_cast<int>(m_position.x),
			static_cast<int>(m_position.y), m_clip, m_angle);
}

void Bullet::update(const double dt) {
	if (m_range <= 0.0) {
		m_is_alive = false;
		return;
	}
	m_position += m_velocity * dt;
	m_range -= m_velocity.lenght() * dt;
}

Bullet::~Bullet() {
	delete m_clip;
	m_clip = nullptr;
}
