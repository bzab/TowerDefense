#include "game.h"

#include "game_state.h"

Game::Game(int width, int height) {
	m_pop = false;
	m_quit = false;

	// Initialize SDL
	if (SDL_Init( SDL_INIT_VIDEO) < 0) {
		std::cerr << "SDL could not initialize! SDL Error: " << SDL_GetError()
				<< std::endl;
		m_quit = true;
	} else { // Create window
		window = SDL_CreateWindow("BZ Tower Defence", SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
		if (window == NULL) {
			std::cerr << "Couldn't open window" << std::endl;
			m_quit = true;
		} else {
			if (SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN) < 0) {
				std::cerr << "Couldn't toggle fullscreen" << std::endl;
				m_quit = true;
			} else {
				// Create renderer for window
				renderer = SDL_CreateRenderer(window, -1,
						SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
				if (renderer == NULL) {
					std::cerr << "Renderer could not be created! SDL Error: "
							<< SDL_GetError() << std::endl;
					m_quit = true;
				} else {
					// Initialize renderer color
					SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF); // 2E

					// Initialize PNG loading
					int imgFlags = IMG_INIT_PNG;
					if (!(IMG_Init(imgFlags) & imgFlags)) {
						std::cerr
								<< "SDL_image could not initialize! SDL_image Error: "
								<< IMG_GetError() << std::endl;
						m_quit = true;
					}

					// Update window size
					SDL_GetWindowSize(window, &m_window_width,
							&m_window_height);

					// Create TextureManager
					texture_manager = new TextureManager(renderer);
				}
			}
		}
	}

}

int Game::getWindowHeight() {
	return m_window_height;
}
int Game::getWindowWidth() {
	return m_window_width;
}

void Game::clear() {
	// Clear states stack
	while (!m_states.empty()) {
		delete m_states.back();
		m_states.pop_back();
	}
}

void Game::changeState(GameState* state) {
	// Cleanup the current state.
	if (!m_states.empty()) {
		delete m_states.back();
		m_states.pop_back();
	}

	// Store and init the new state.
	m_states.push_back(state);
}

void Game::togglePopState() {
	m_pop = true;
}

// Leave current state and go to previous state.
void Game::popState() {
	if (!m_states.empty()) {
		delete m_states.back();
		m_states.pop_back();
	}

	if (!m_states.empty())
		m_states.back()->resume();
}

// Pause the current state and go to a new state.
void Game::pushState(GameState* state) {
	if (!m_states.empty())
		m_states.back()->pause();

	m_states.push_back(state);
}

void Game::quit() {
	m_quit = true;
}

GameState* Game::currentState() {
	if (m_states.empty())
		return nullptr;
	else
		return m_states.back();
}

void Game::gameLoop() {
	// TODO: Add clock

	while (!m_quit) {
		// TODO: Control frame rate -> elapsed dt?

		// Exception handling
		if (currentState() == nullptr) {
			std::cerr << "Exception in Loop" << std::endl;
			continue;
		}

		m_pop = false;

		// Get user input for current game state
		currentState()->handleInput();

		//currentState()->update(dt);
		currentState()->update(0.01);

		SDL_RenderClear(renderer);
		currentState()->draw();
		SDL_RenderPresent(renderer);

		if (m_pop) {
			popState();
		}
	}
}

Game::~Game() {
	m_states.clear();

	delete texture_manager;
	texture_manager = nullptr;

	SDL_DestroyRenderer(renderer);
	renderer = nullptr;
	SDL_DestroyWindow(window);
	window = nullptr;
	IMG_Quit();
	SDL_Quit();
}
