#include "remove_tower_icon.h"

RemoveTowerIcon::RemoveTowerIcon(int x, int y, const std::string &path,
		TextureManager* tm, int index_in_atlas, int tex_size) {
	m_is_active = false;
	m_is_clicked = false;
	m_is_hovered = false;
	m_position.x = x;
	m_position.y = y;
	m_texture_manager = tm;
	m_sprite = tm->get(path);

	m_clip = new SDL_Rect;
	m_clip->x = ((index_in_atlas) % (m_sprite->getWidth() / tex_size))
			* tex_size;
	m_clip->y = ((index_in_atlas) / (m_sprite->getHeight() / tex_size))
			* tex_size;
	m_clip->h = tex_size;
	m_clip->w = tex_size;
}

bool RemoveTowerIcon::isActive() {
	return m_is_active;
}

void RemoveTowerIcon::flipActive() {
	m_is_active = !m_is_active;
}

void RemoveTowerIcon::setActive(bool f) {
	m_is_active = f;
}

void RemoveTowerIcon::onClick() {
	flipActive();
}

void RemoveTowerIcon::render() {
	if (m_is_hovered) {
		m_sprite->setColor(200, 200, 200);
	} else {
		m_sprite->setColor(255, 255, 255);
	}

	m_sprite->render(m_position.x, m_position.y, m_clip);

	if (m_is_active) {
		SDL_Rect active_clip = { 0, 0, m_clip->w, m_clip->h };
		m_sprite->render(m_position.x, m_position.y, &active_clip);
	}
}
