#include "clickable_gui_object.h"

bool ClickableGUIObject::isClicked() {
	return m_is_clicked;
}

int ClickableGUIObject::getHeight() {
	return m_clip->h;
}

int ClickableGUIObject::getWidth() {
	return m_clip->w;
}

void ClickableGUIObject::handleEvent(SDL_Event* e) {
	if (e->type == SDL_MOUSEMOTION || e->type == SDL_MOUSEBUTTONDOWN) {
		m_is_clicked = false;
		// Get mouse position
		int x = 0;
		int y = 0;
		;
		SDL_GetMouseState(&x, &y);

		// Check if mouse is in button
		bool inside = true;

		if (x < m_position.x + 1) {
			inside = false;
		} else if (x > m_position.x + getWidth() - 1) {
			inside = false;
		} else if (y < m_position.y + 1) {
			inside = false;
		} else if (y > m_position.y + getHeight() - 1) {
			inside = false;
		}

		if (!inside) {
			offHover();
		} else {
			// Mouse over sprite
			switch (e->type) {
			case SDL_MOUSEMOTION:
				onHover();
				break;

			case SDL_MOUSEBUTTONDOWN:
				m_is_clicked = true;
				onClick();
				break;

			default:
				//
				break;
			}
		}
	}
}

void ClickableGUIObject::onHover() {
	m_is_hovered = true;
}

void ClickableGUIObject::offHover() {
	m_is_hovered = false;
}
