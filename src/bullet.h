#pragma once

#include <vector>

#include "moving_entity.h"
#include "sdl_includes.h"

class Bullet: public MovingEntity {
public:
	Bullet(double x, double y, const std::string &path, TextureManager* tm,
			double angle, double range, double spd, int dmg);
	Bullet(const Bullet &) = delete;
	Bullet& operator=(const Bullet &) = delete;

	int getDmg();

	void destroy();
	void render();
	void update(double dt);

	~Bullet();
private:
	double m_range;

	int m_dmg;
};
