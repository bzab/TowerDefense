template<typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

template<typename T> int sgn_eps(T val, T eps) {
	return (eps < val) - (val < -eps);
}
