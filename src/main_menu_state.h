#pragma once

#include <vector>

#include "game_state.h"
#include "edit_state.h"
#include "game_state.h"
#include "play_state.h"

#include "button.h"
#include "edit_button.h"
#include "exit_button.h"
#include "play_button.h"

class MainMenuState: public GameState {
public:
	MainMenuState(Game* game);
	MainMenuState(const MainMenuState &) = delete;
	MainMenuState& operator=(const MainMenuState &) = delete;

	void pause();
	void resume();
	void draw();
	void update(const double dt);
	void handleInput();

	~MainMenuState();

private:

	Texture* m_img;
	std::vector<Button*> m_buttons;
};
