#pragma once

#include "button.h"
#include "map.h"
#include "tile_selection_bar.h"

class SwapButton: public Button {
public:
	SwapButton(int, int, const std::string &, Game*, Map*, TileSelectionBar*);
	SwapButton(const SwapButton &) = delete;
	SwapButton& operator=(const SwapButton &) = delete;

	void onClick();
private:
	Map* m_map;
	TileSelectionBar* m_tile_bar;
};
