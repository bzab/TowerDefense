#include "edit_state.h"

EditState::EditState(Game* g) {
	game = g;

	m_img = game->texture_manager->get("img/test_edit.png");
	m_map = new EditStateMap(200, 200, 32, "maps/test_map.map",
			"img/tiles/tile_atlas32.png", game);
	m_tile_bar = new TileSelectionBar((game->getWindowWidth() - 160) / 2, 260,
			"img/tilebar.png", game, m_map, 24, 32);

	m_buttons.push_back(
			new SaveMapButton((game->getWindowWidth() - 64) / 2 - 35, 510,
					"img/buttons/btn_save2.png", game, m_map));
	m_buttons.push_back(
			new LoadMapButton((game->getWindowWidth() - 64) / 2 + 35, 510,
					"img/buttons/btn_load2.png", game, m_map));
	m_buttons.push_back(
			new MenuButton((game->getWindowWidth() - 152) / 2, 800,
					"img/buttons/btn_menu.png", game));
	m_buttons.push_back(
			new SwapButton((game->getWindowWidth() - 152) / 2, 150,
					"img/buttons/btn_swap.png", game, m_map, m_tile_bar));

}

void EditState::handleInput() {
	SDL_Event events;
	while (SDL_PollEvent(&events) != 0) {
		if (events.type == SDL_QUIT) {
			game->quit();
		} else if ((events.type == SDL_MOUSEMOTION)
				|| (events.type == SDL_MOUSEBUTTONDOWN)
				|| (events.type == SDL_MOUSEBUTTONUP)
				|| (events.type == SDL_MOUSEWHEEL)) {
			for (auto &button : m_buttons) {
				button->handleEvent(&events);
			}
			m_map->handleEvent(&events);
			m_tile_bar->handleEvent(&events);
		}
	} // End while
}

void EditState::update(const double dt) {
}

void EditState::draw() {
	m_img->render((game->getWindowWidth() - m_img->getWidth()) / 2, 0);
	for (auto &button : m_buttons) {
		button->render();
	}
	m_map->render();
	m_tile_bar->render();
}

void EditState::pause() {
}

void EditState::resume() {
}

EditState::~EditState() {
	while (!m_buttons.empty()) {
		delete m_buttons.back();
		m_buttons.pop_back();
	}
	delete m_map;
	m_map = nullptr;
}
