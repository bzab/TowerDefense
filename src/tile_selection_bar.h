#pragma once

#include <vector>

#include "selection_bar.h"
#include "map.h"

class TileSelectionBar: public SelectionBar {
public:
	TileSelectionBar(int x, int y, const std::string &path, Game* game,
			Map* map, int num_of_tiles, int tile_size);
	TileSelectionBar(const TileSelectionBar &) = delete;
	TileSelectionBar& operator=(const TileSelectionBar &) = delete;

	int getSelectedTileId();
	void handleEvent(SDL_Event* e);
	void render();

	~TileSelectionBar();

private:
	Map* m_map;
	std::vector<Tile*> m_tiles;
};
