#pragma once

#include <string>
#include <iostream>

#include "sdl_includes.h"

class Texture {
public:
	Texture(const std::string &path, SDL_Renderer* renderer);
	Texture(const Texture &) = delete;
	Texture& operator=(const Texture &) = delete;

#ifdef _SDL_TTF_H
	// Creates image from font string
	bool loadFromRenderedText( std::string textureText, SDL_Color textColor );
#endif

	// Set color modulation
	void setColor(Uint8 red, Uint8 green, Uint8 blue);

	// Set blending
	void setBlendMode(SDL_BlendMode blending);

	// Set alpha modulation
	void setAlpha(Uint8 alpha);

	// Renders texture at given point
	void render(int x, int y, SDL_Rect* clip = NULL, double angle = 0.0,
			SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

	// Renders texture with added shadow below
	void renderWithShadow(Uint8 shadow_size, Uint8 alpha, int x, int y,
			SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL,
			SDL_RendererFlip flip = SDL_FLIP_NONE);

	// Gets image dimensions
	int getWidth();
	int getHeight();

	~Texture();

private:
	SDL_Renderer* m_renderer;
	SDL_Texture* m_texture;

	int m_width;
	int m_height;

	bool loadFromFile(const std::string &path);

	void free();
};
