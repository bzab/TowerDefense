#include "play_state.h"

PlayState::PlayState(Game* g) {
	game = g;

	m_buttons.push_back(
			new MenuButton((game->getWindowWidth() - 152) / 2, 800,
					"img/buttons/btn_menu.png", game));
	m_img = game->texture_manager->get("img/test_play.png");

	m_tower_bar = new TowerSelectionBar((game->getWindowWidth() - 160) / 2, 260,
			"img/tilebar.png", "img/turrets/turret_atlas32.png", game, 32);
	m_map = new PlayStateMap(200, 200, 32, "maps/test_map.map",
			"img/tiles/tile_atlas32.png", game, m_tower_bar);
}

void PlayState::draw() {
	m_img->render((game->getWindowWidth() - m_img->getWidth()) / 2, 0);
	m_map->render();
	for (auto &button : m_buttons) {
		button->render();
	}
	m_tower_bar->render();
}

void PlayState::update(const double dt) {
	m_map->update(dt);
}

void PlayState::handleInput() {
	SDL_Event events;
	while (SDL_PollEvent(&events) != 0) {
		if (events.type == SDL_QUIT) {
			game->quit();
		} else if ((events.type == SDL_MOUSEMOTION)
				|| (events.type == SDL_MOUSEBUTTONDOWN)
				|| (events.type == SDL_MOUSEBUTTONUP)
				|| (events.type == SDL_MOUSEWHEEL)) {
			for (auto &button : m_buttons) {
				button->handleEvent(&events);
			}
			m_map->handleEvent(&events);
			m_tower_bar->handleEvent(&events);
		}
	} // End while
}

void PlayState::pause() {
}

void PlayState::resume() {
}

PlayState::~PlayState() {
	while (!m_buttons.empty()) {
		delete m_buttons.back();
		m_buttons.pop_back();
	}
	delete m_map;
	m_map = nullptr;
}
