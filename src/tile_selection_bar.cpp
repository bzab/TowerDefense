#include "tile_selection_bar.h"

TileSelectionBar::TileSelectionBar(int x, int y, const std::string &path,
		Game* game, Map* map, int num_of_tiles, int tile_size) :
		SelectionBar(x, y, path, game) {
	m_map = map;

	int n = 1;
	const int width_in_tiles = 4;
	SDL_Point new_tile_pos = { m_position.x + 7, m_position.y + 7 };

	while (n <= num_of_tiles) {
		m_tiles.push_back(
				new Tile(new_tile_pos.x, new_tile_pos.y, n, tile_size,
						m_map->getAtlasPath(), game));
		if (n % width_in_tiles) {
			new_tile_pos.x += 6 + tile_size;
		} else {
			new_tile_pos.x = m_position.x + 7;
			new_tile_pos.y += 6 + tile_size;
		}
		++n;
	}
}

int TileSelectionBar::getSelectedTileId() {
	int tiles_size = m_tiles.size();
	for (int i = 0; i < tiles_size; ++i) {
		if (m_tiles[i]->isActive()) {
			return i + 1;
		}
	}
	return 0; //Error!
}

void TileSelectionBar::handleEvent(SDL_Event* e) {
	for (auto &tile : m_tiles) {
		tile->handleEvent(e);
		if (tile->isActive() && tile->isClicked()) {
			// Deactivate other tiles in bar
			for (auto &t : m_tiles) {
				t->setActive(t->isClicked());
			}
		}
	}
}

void TileSelectionBar::render() {
	m_sprite->render(m_position.x, m_position.y, m_clip);
	for (auto &tile : m_tiles) {
		tile->render();
	}
}

TileSelectionBar::~TileSelectionBar() {
	for (auto &tile : m_tiles) {
		delete tile;
	}
	m_tiles.clear();
}
