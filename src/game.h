#pragma once

#include <iostream>
#include <vector>

#include "sdl_includes.h"
#include "texture_manager.h"

class GameState;

class Game {
public:

	Game(int width, int height);
	Game(const Game &) = delete;
	Game& operator=(const Game &) = delete;
	~Game();

	int getWindowHeight();
	int getWindowWidth();

	// Functions for changing game state
	void clear();
	void changeState(GameState* state);
	void popState();
	void pushState(GameState* state);
	void togglePopState();

	void quit(); // Set quit to true

	// Returns pointer to current state
	GameState* currentState();

	void gameLoop();

	TextureManager* texture_manager = nullptr;

	SDL_Renderer* renderer;
	SDL_Window* window;

private:
	std::vector<GameState*> m_states;

	bool m_pop; // Pop state flag
	bool m_quit; // Quit flag

	int m_window_height;
	int m_window_width;
};
