#include "map.h"

Map::Map(int x, int y, int tile_size, const std::string &path,
		const std::string &atlas_path, Game* game) {
	m_game = game;
	m_atlas_path = atlas_path;
	m_file_path = path;
	m_position.x = x;
	m_position.y = y;

	loadMap(tile_size, m_file_path);
}

void Map::clearMap() {
	for (auto &row : m_tiles) {
		for (auto &tile : row) {
			delete tile;
		}
	}
	m_tiles.clear();
	for (auto &spawn : m_spawns) {
		delete spawn;
	}
	m_spawns.clear();
}

std::string& Map::getAtlasPath() {
	return m_atlas_path;
}

std::string& Map::getMapPath() {
	return m_file_path;
}

void Map::loadMap(int tile_size, const std::string &path) {
	clearMap();

	//Open the map
	std::ifstream map_file(path, std::ifstream::in);

	//If the map couldn't be loaded
	if (!map_file.is_open()) {
		std::cerr << "Unable to load map file:\n" << path << std::endl;
		m_game->quit();
	} else {
		int map_width = -1;
		int map_height = -1;
		map_file >> map_width;
		if (map_file.fail()) {
			std::cerr << "Missing map_width in file: " << path << std::endl;
			m_game->quit();
		}
		map_file >> map_height;
		if (map_file.fail()) {
			std::cerr << "Missing map_height in file: " << path << std::endl;
			m_game->quit();
		}
		bool break_flag = false;
		for (int i = 0; i < map_height; ++i) {
			std::vector<Tile *> row;
			for (int j = 0; j < map_width; ++j) {
				int tile_id = -1;
				map_file >> tile_id;
				if (map_file.fail()) {
					std::cerr << "Wrong map size in file: " << path
							<< std::endl;
					m_game->quit();
					break_flag = true;
					break;
				} else {
					row.push_back(
							new Tile(m_position.x + j * tile_size,
									m_position.y + i * tile_size, tile_id,
									tile_size, m_atlas_path, m_game));
				}
			}
			m_tiles.push_back(row);
			if (break_flag) {
				break;
			}
		}

		int number_of_spawns = -1;
		map_file >> number_of_spawns;
		if (number_of_spawns >= 0) {
			for (int i = 0; i < number_of_spawns; ++i) {
				SDL_Point* spawn = new SDL_Point { -1, -1 };
				map_file >> spawn->x;
				map_file >> spawn->y;
				if (spawn->x >= 0 && spawn->y >= 0) {
					m_spawns.push_back(spawn);
				} else {
					delete spawn;
				}
			}
		} else {
			std::cerr << "Wrong number of spawns (" << number_of_spawns
					<< " in file: " << path << std::endl;
			m_game->quit();
		}

		map_file.close();
	}
}

void Map::render() {
	for (auto &row : m_tiles) {
		for (auto &tile : row) {
			tile->render();
		}
	}
}

Map::~Map() {
	clearMap();
}
