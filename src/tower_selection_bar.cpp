#include "tower_selection_bar.h"

TowerSelectionBar::TowerSelectionBar(int x, int y, const std::string &bar_path,
		const std::string &towers_path, Game* game, int tile_size) :
		SelectionBar(x, y, bar_path, game) {

	m_remove = false;

	SDL_Point new_tile_pos = { m_position.x + 7, m_position.y + 7 };

	m_remove_tower_icon = new RemoveTowerIcon(new_tile_pos.x, new_tile_pos.y,
			towers_path, game->texture_manager, 1, tile_size);
	m_towers.push_back(
			new Tower(new_tile_pos.x + 6 + tile_size, new_tile_pos.y,
					towers_path, game->texture_manager, 2, tile_size, 1, 2, 30,
					320.0, 300.0, 8.0));
	m_towers.push_back(
			new Tower(new_tile_pos.x + 2 * (6 + tile_size), new_tile_pos.y,
					towers_path, game->texture_manager, 4, tile_size, 2, 3, 50,
					288.0, 300.0, 6.0));
}

Tower* TowerSelectionBar::getNewTower(SDL_Point pos) {
	Tower* t_ptr = getSelectedTower();
	if (t_ptr) {
		return new Tower(pos.x, pos.y, *t_ptr);
	} else {
		return nullptr;
	}
}

Tower* TowerSelectionBar::getSelectedTower() {
	for (auto &tower : m_towers) {
		if (tower->isActive()) {
			return tower;
		}
	}
	std::cerr << "Selected Tower is nullptr!" << std::endl;
	return nullptr; //Error!
}

bool TowerSelectionBar::isRemove() {
	return m_remove;
}

void TowerSelectionBar::handleEvent(SDL_Event* e) {
	m_remove_tower_icon->handleEvent(e);
	m_remove = m_remove_tower_icon->isActive();
	// If Remove Tower was selected -> Deactivate towers in bar
	if (m_remove_tower_icon->isActive() && m_remove_tower_icon->isClicked()) {
		for (auto &tower : m_towers) {
			tower->setActive(false);
		}
	}

	for (auto &tower : m_towers) {
		tower->handleEvent(e);
		// Disable all active tiles in bar except clicked one
		if (tower->isActive() && tower->isClicked()) {
			for (auto &t : m_towers) {
				t->setActive(t->isClicked());
			}
			m_remove_tower_icon->setActive(false);
		}
	}
}

void TowerSelectionBar::render() {
	m_sprite->render(m_position.x, m_position.y, m_clip);
	m_remove_tower_icon->render();
	for (auto &tower : m_towers) {
		tower->render();
	}
}

TowerSelectionBar::~TowerSelectionBar() {
	delete m_remove_tower_icon;
	m_remove_tower_icon = nullptr;
	for (auto &tower : m_towers) {
		delete tower;
	}
	m_towers.clear();
}
