#include "play_button.h"

#include "game.h"

void PlayButton::onClick() {
	m_game->pushState(new PlayState(m_game));
}
