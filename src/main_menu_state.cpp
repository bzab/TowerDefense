#include "main_menu_state.h"

MainMenuState::MainMenuState(Game* g) {
	game = g;

	m_img = game->texture_manager->get("img/test_menu.png");
	m_buttons.push_back(
			new PlayButton((game->getWindowWidth() - 152) / 2, 150,
					"img/buttons/btn_play.png", game));
	m_buttons.push_back(
			new EditButton((game->getWindowWidth() - 152) / 2, 250,
					"img/buttons/btn_edit.png", game));
	m_buttons.push_back(
			new ExitButton(game->getWindowWidth() - 152, 400,
					"img/buttons/btn_quit.png", game));
}

void MainMenuState::handleInput() {
	SDL_Event events;
	while (SDL_PollEvent(&events) != 0) {
		if (events.type == SDL_QUIT) {
			game->quit();
		} else if ((events.type == SDL_MOUSEMOTION)
				|| (events.type == SDL_MOUSEBUTTONDOWN)
				|| (events.type == SDL_MOUSEBUTTONUP)
				|| (events.type == SDL_MOUSEWHEEL)) {
			for (auto &button : m_buttons) {
				button->handleEvent(&events);
			}
		}
	} // End while
}

void MainMenuState::update(const double dt) {

}

void MainMenuState::draw() {
	m_img->render((game->getWindowWidth() - m_img->getWidth()) / 2, 20);
	for (auto &button : m_buttons) {
		button->render();
	}
}

void MainMenuState::pause() {
}

void MainMenuState::resume() {
}

MainMenuState::~MainMenuState() {
	while (!m_buttons.empty()) {
		delete m_buttons.back();
		m_buttons.pop_back();
	}
}
