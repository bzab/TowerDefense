#include "load_map_button.h"

#include "map.h"

LoadMapButton::LoadMapButton(int x, int y, const std::string &path, Game* game,
		Map* map) :
		Button(x, y, path, game) {
	m_map = map;
}

void LoadMapButton::onClick() {
	m_map->loadMap(32, m_map->getMapPath());
}
