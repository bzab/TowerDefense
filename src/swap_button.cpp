#include "swap_button.h"

#include "map.h"

SwapButton::SwapButton(int x, int y, const std::string &path, Game* game,
		Map* map, TileSelectionBar* tile_bar) :
		Button(x, y, path, game) {
	m_map = map;
	m_tile_bar = tile_bar;
}

void SwapButton::onClick() {
	int tile_id = m_tile_bar->getSelectedTileId();
	if (tile_id > 0 && tile_id < 24) {
		m_map->setSwap(tile_id);
	}
}
