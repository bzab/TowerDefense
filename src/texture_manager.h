#pragma once

#include <map>
#include <string>

#include "texture.h"

class TextureManager {
public:
	TextureManager(SDL_Renderer* r);
	TextureManager(const TextureManager &) = delete;
	TextureManager& operator=(const TextureManager &) = delete;

	SDL_Renderer* renderer;

	// Get texture
	Texture* get(const std::string &path);

	// Remove all textures
	void purge();

	~TextureManager();
private:
	// Texture Dict
	std::map<std::string, Texture*> m_textures;
	std::map<std::string, Texture*>::iterator m_iter;

	// Load texture
	Texture* loadFromFile(const std::string &path);
};
