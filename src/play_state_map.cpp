#include "play_state_map.h"

#include "directions.h"
#include "geo_2d.h"

PlayStateMap::PlayStateMap(int x, int y, int tile_size, const std::string &path,
		const std::string &atlas_path, Game* game, TowerSelectionBar* tsb) :
		Map(x, y, tile_size, path, atlas_path, game) {
	m_spawn_timer = 0.0;
	resetSpawnTimer();
	m_tower_bar = tsb;
	m_iter_b = m_bullets.begin();
	m_iter_e = m_enemies.begin();

	for (auto &row : m_tiles) {
		std::vector<uint8_t> roads_row;
		for (auto &tile : row) {
			// Map connected road tiles
			const int first_id = 3; // Id of first road tile in atlas
			uint8_t tile_connections = 0;
			switch (tile->getTileId()) {
			case first_id:
				tile_connections |= NORTH | SOUTH;
				break;
			case first_id + 1:
				tile_connections |= WEST | EAST;
				break;
			case first_id + 2:
				tile_connections |= SOUTH | EAST;
				break;
			case first_id + 3:
				tile_connections |= NORTH | EAST;
				break;
			case first_id + 4:
				tile_connections |= NORTH | WEST;
				break;
			case first_id + 5:
				tile_connections |= SOUTH | WEST;
				break;
			case first_id + 6:
				tile_connections |= NORTH | WEST | SOUTH | EAST;
				break;
			case first_id + 7:
				tile_connections |= NORTH | WEST | SOUTH | EAST;
				break;
			case first_id + 8:
				tile_connections |= NORTH | SOUTH;
				break;
			case first_id + 9:
				tile_connections |= WEST | EAST;
				break;
			default:
				break;
			}
			roads_row.push_back(tile_connections);
		}
		m_roads.push_back(roads_row);
	}
}

SDL_Point PlayStateMap::getPosition() {
	return m_position;
}

uint8_t PlayStateMap::getAvailableDirections(unsigned int x, unsigned int y) {
	if ((x < m_roads.size()) && (y < m_roads[x].size())) {
		return m_roads[y][x];
	}
	return 0;
}

int PlayStateMap::getVectorSizeX() {
	return m_tiles[0].size();
}

int PlayStateMap::getVectorSizeY() {
	return m_tiles.size();
}

void PlayStateMap::handleEvent(SDL_Event* e) {
	for (auto &row : m_tiles) {
		for (auto &tile : row) {
			tile->handleEvent(e);
			if (tile->isClicked()) {
				for (auto &row : m_tiles) {
					for (auto &t : row) {
						if (t->isActive() && !t->isClicked()) {
							t->setActive(false);
						}
					}

					// Grass tile id = 2
					if (tile->getTileId() == 2) {
						if (m_tower_bar->isRemove()) {
							tile->removeTower();
						} else {
							Tower* tower_ptr = m_tower_bar->getNewTower(
									tile->getPosition());
							if (tower_ptr) {
								tile->setTower(*tower_ptr);
								tile->setActive(false);
							}
						}
					}
				}
			}
		}
	}
}

void PlayStateMap::removeDeadBullets() {
	m_iter_b = m_bullets.begin();
	while (m_iter_b != m_bullets.end()) {
		if (!(*m_iter_b)->isAlive()) {
			delete *m_iter_b;
			m_iter_b = m_bullets.erase(m_iter_b);  // erase(iter); ++iter
		} else {
			++m_iter_b;
		}
	}
}

void PlayStateMap::removeDeadEnemies() {
	m_iter_e = m_enemies.begin();
	while (m_iter_e != m_enemies.end()) {
		if (!(*m_iter_e)->isAlive()) {
			delete *m_iter_e;
			m_iter_e = m_enemies.erase(m_iter_e);  // erase(iter); ++iter
		} else {
			++m_iter_e;
		}
	}
}

void PlayStateMap::render() {
	Map::render();
	for (auto &enemy : m_enemies) {
		enemy->render();
	}
	for (auto &bullet : m_bullets) {
		bullet->render();
	}
}

void PlayStateMap::resetSpawnTimer() {
	m_spawn_timer = 5.0;
}

void PlayStateMap::spawnEnemy(int x, int y, int index_in_atlas, int tex_size,
		int textures_num, double v, int hp, int hb_x, int hb_y, int hb_w,
		int hb_h) {
	m_enemies.push_back(
			new Enemy(x, y, "img/enemies/enemy_atlas32.png",
					m_game->texture_manager, this, index_in_atlas, tex_size,
					textures_num, v, hp, hb_x, hb_y, hb_w, hb_h));
}

void PlayStateMap::spawnBullet(double x, double y, double angle, double range,
		double spd, int dmg) {
	m_bullets.push_back(
			new Bullet(x, y, "img/turrets/bullet.png", m_game->texture_manager,
					angle, range, spd, dmg));
}

void PlayStateMap::update(double dt) {
	m_spawn_timer -= dt;

	// Spawn new enemies
	if (m_spawn_timer < 0.0) {
		resetSpawnTimer(); // Set waiting time for next enemy
		m_spawn_timer += static_cast<double>(std::rand() % 10);
		// Choose spawnpoint
		int n = std::rand() % m_spawns.size();
		spawnEnemy(m_spawns[n]->x, m_spawns[n]->y, 0, 32, 4, 10.0, 100, 9, 8,
				14, 20);
	}

	for (auto &enemy : m_enemies) {
		enemy->update(dt, this);
	}
	for (auto &row : m_tiles) {
		for (auto &tile : row) {
			if (tile->hasTower()) {
				Tower* t = tile->getTower();
				t->updateTimer(dt);
				for (auto &enemy : m_enemies) {
					if (enemy->isAlive()) {
						Point target = enemy->getPosition();
						Vec2D<double> aim(t->getPosition(), target);
						if (t->update(dt, aim, enemy->getVelocity(), this)) { // If shot, break
							break;
						}
					}
				}
			}
		}
	}
	for (auto &bullet : m_bullets) {
		bullet->update(dt);

		if (bullet->isAlive()) {
			for (auto &enemy : m_enemies) {
				// y+1 -> Take position of bullets center
				SDL_Point bul_cur = { static_cast<int>(bullet->getPosition().x),
						static_cast<int>(bullet->getPosition().y) + 1 };
				SDL_Point bul_prev =
						{
								bul_cur.x
										- static_cast<int>(bullet->getVelocity().x
												* dt),
								bul_cur.y
										- static_cast<int>(bullet->getVelocity().y
												* dt) };
				SDL_Rect hitbox = *enemy->getHitbox();
				hitbox.x += static_cast<int>(enemy->getPosition().x);
				hitbox.y += static_cast<int>(enemy->getPosition().y);

				// Check collision
				if (vectInRect(bul_prev, bul_cur, hitbox)) {
					bullet->destroy();
					enemy->damage(bullet->getDmg());
					break;
				}

			}
		}
	}

	removeDeadEnemies();
	removeDeadBullets();
}

PlayStateMap::~PlayStateMap() {
	m_iter_b = m_bullets.begin();
	while (m_iter_b != m_bullets.end()) {
		delete *m_iter_b;
		m_iter_b = m_bullets.erase(m_iter_b);  // erase(iter); ++iter
	}

	m_iter_e = m_enemies.begin();
	while (m_iter_e != m_enemies.end()) {
		delete *m_iter_e;
		m_iter_e = m_enemies.erase(m_iter_e);  // erase(iter); ++iter
	}
}
