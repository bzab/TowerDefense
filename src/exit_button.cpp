#include "exit_button.h"

#include <iostream>

#include "game.h"

void ExitButton::onClick() {
	m_game->quit();
}
