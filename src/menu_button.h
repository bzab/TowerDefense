#pragma once

#include "button.h"

class MenuButton: public Button {
public:
	using Button::Button;

	void onClick();
};
