#pragma once

#include <string>

#include "game.h"
#include "gui_object.h"

class SelectionBar: public GUIObject {
public:
	SelectionBar(int x, int y, const std::string &path, Game* game);
	SelectionBar(const SelectionBar &) = delete;
	SelectionBar& operator=(const SelectionBar &) = delete;

	virtual void handleEvent(SDL_Event* e) {
	}
	;
	virtual void render();

	virtual ~SelectionBar();

protected:
	SDL_Point m_position;
	SDL_Rect* m_clip;

	Texture* m_sprite;
	TextureManager* m_texture_manager;
};
