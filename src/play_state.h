#pragma once

#include <vector>

#include "game_state.h"

#include "button.h"
#include "play_state_map.h"
#include "menu_button.h"
#include "tower_selection_bar.h"

class PlayState: public GameState {
public:
	PlayState(Game* game);
	PlayState(const PlayState &) = delete;
	PlayState& operator=(const PlayState &) = delete;

	void pause();
	void resume();
	void draw();
	void update(const double dt);
	void handleInput();

	~PlayState();

private:

	Texture* m_img;
	TowerSelectionBar* m_tower_bar;
	PlayStateMap* m_map;
	std::vector<Button*> m_buttons;
};
