#pragma once

#include "sdl_includes.h"
#include "texture.h"
#include "texture_manager.h"

class GUIObject {
public:

	virtual void render()=0;
	//virtual void setPosition(int, int);

	SDL_Point getPosition() {
		return m_position;
	}
	;
	SDL_Rect getClip() {
		return *m_clip;
	}
	;

	virtual ~GUIObject() {
	}
	;

protected:
	SDL_Point m_position;
	SDL_Rect* m_clip;

	Texture* m_sprite;
	TextureManager* m_texture_manager;
};
