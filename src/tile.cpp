#include "tile.h"

Tile::Tile(int x, int y, int n, int size, const std::string &path, Game* game) {
	m_id = n;
	m_is_active = false;
	m_is_clicked = false;
	m_is_hovered = false;
	m_position.x = x;
	m_position.y = y;
	m_texture_manager = game->texture_manager;
	m_tower = nullptr;
	m_sprite = m_texture_manager->get(path);

	if (n <= 0
			|| n
					>= ((m_sprite->getWidth() / size)
							* (m_sprite->getHeight() / size))) {
		std::cerr << "Wrong tile ID: " << n << std::endl;
		game->quit();
	}

	m_clip = new SDL_Rect;

	m_clip->x = (n % (m_sprite->getWidth() / size)) * size;
	m_clip->y = (n / (m_sprite->getHeight() / size)) * size;
	m_clip->h = size;
	m_clip->w = size;
}

bool Tile::isActive() {
	return m_is_active;
}

bool Tile::hasTower() {
	return m_tower != nullptr;
}

void Tile::flipActive() {
	m_is_active = !m_is_active;
}

void Tile::setActive(bool f) {
	m_is_active = f;
}

int Tile::getTileId() {
	return m_id;
}

void Tile::onClick() {
	flipActive();
}

void Tile::removeTower() {
	if (m_tower) {
		delete m_tower;
	}
	m_tower = nullptr;
}

void Tile::render() {
	if (m_is_hovered) {
		m_sprite->setColor(200, 200, 200);
	} else {
		m_sprite->setColor(255, 255, 255);
	}
	m_sprite->render(m_position.x, m_position.y, m_clip);

	if (m_tower) {
		m_tower->render();
	}

	if (m_is_active) {
		SDL_Rect active_clip = { 0, 0, m_clip->w, m_clip->h };
		m_sprite->render(m_position.x, m_position.y, &active_clip);
	}
}

void Tile::setTower(const Tower &tower) {
	if (m_tower) {
		delete m_tower;
	}
	m_tower = new Tower(m_position.x, m_position.y, tower);
}

Tower* Tile::getTower() {
	return m_tower;
}

Tile::~Tile() {
	delete m_clip;
	removeTower();
}
