#pragma once

#include <iostream>
#include "game.h"

class GameState {
public:
	Game* game;

	// Changing states causes pausing/resuming.
	// TODO rework or delete pause/resume methods
	virtual void pause() {
	}
	;
	virtual void resume() {
	}
	;

	virtual void draw() = 0;
	virtual void update(const double dt) = 0;
	virtual void handleInput() = 0;

	virtual ~GameState() {
	}
	;
};
