#pragma once

#include <vector>

#include "directions.h"
#include "moving_entity.h"
#include "play_state_map.h"
#include "sdl_includes.h"

class PlayStateMap;

class Enemy: public MovingEntity {
public:
	Enemy(double x, double y, const std::string &path, TextureManager* tm,
			PlayStateMap* map, int index_in_atlas, int tex_size,
			int textures_num, double v, int hp, int hb_x, int hb_y, int hb_w,
			int hb_h);
	Enemy(const Enemy &) = delete;
	Enemy& operator=(const Enemy &) = delete;

	SDL_Rect* getHitbox();

	void damage(int dmg);
	void render();
	void update(double dt, PlayStateMap* map);

	~Enemy();
private:
	std::vector<SDL_Rect*> m_clips;

	int m_hp;

	SDL_Rect* m_hitbox;

	uint8_t m_direction;
};
