#pragma once

#include "geo_2d.h"
#include "gui_object.h"
#include "sdl_includes.h"
#include "texture_manager.h"

class MovingEntity: public GUIObject {
public:
	// Move
	bool isAlive();
	Vec2D<double> & getVelocity();

	Point getPosition();

protected:
	bool m_is_alive;
	double m_angle;
	Point m_position;
	Vec2D<double> m_velocity; // px/s

};
