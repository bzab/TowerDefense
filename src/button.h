#pragma once

#include <string>

#include "clickable_gui_object.h"

class Button: public ClickableGUIObject {
public:
	Button(int x, int y, const std::string &path, Game* game);
	Button(const Button &) = delete;
	Button& operator=(const Button &) = delete;

	virtual void onClick();
	virtual void render();

	virtual ~Button();
};
