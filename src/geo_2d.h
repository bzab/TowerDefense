#pragma once

#include <cmath>

#include "sdl_includes.h"

// Point

typedef struct Point {
	double x;
	double y;
} Point;

inline bool operator==(Point const &p1, Point const &p2) {
	return (p1.x == p2.x) && (p1.y == p2.y);
}

inline bool operator!=(Point const &p1, Point const &p2) {
	return !(p1 == p2);
}

inline bool operator==(SDL_Point const &p1, Point const &p2) {
	return (p1.x == p2.x) && (p1.y == p2.y);
}

inline bool operator!=(SDL_Point const &p1, Point const &p2) {
	return !(p1 == p2);
}

inline bool operator==(Point const &p1, SDL_Point const &p2) {
	return (p1.x == p2.x) && (p1.y == p2.y);
}

inline bool operator!=(Point const &p1, SDL_Point const &p2) {
	return !(p1 == p2);
}

inline double distance(Point const &p1, Point const &p2) {
	return sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}

inline double distance(SDL_Point const &p1, Point const &p2) {
	return sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}

inline double distance(Point const &p1, SDL_Point const &p2) {
	return sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}

// Vec2D

template<class T>
class Vec2D {
public:
	T x;
	T y;

	Vec2D() :
			x(0), y(0) {
	}
	;
	Vec2D(T x, T y) :
			x(x), y(y) {
	}
	;
	Vec2D(const Vec2D &v) :
			x(v.x), y(v.y) {
	}
	;
	Vec2D(const SDL_Point &p1, const SDL_Point &p2) :
			x(p2.x - p1.x), y(p2.y - p1.y) {
	}
	;
	Vec2D(const SDL_Point &p1, const Point &p2) :
			x(p2.x - p1.x), y(p2.y - p1.y) {
	}
	;
	Vec2D(const Point &p1, const SDL_Point &p2) :
			x(p2.x - p1.x), y(p2.y - p1.y) {
	}
	;
	Vec2D(const Point &p1, const Point &p2) :
			x(p2.x - p1.x), y(p2.y - p1.y) {
	}
	;

	Vec2D& operator=(const Vec2D& v) {
		x = v.x;
		y = v.y;
		return *this;
	}
	Vec2D operator+(Vec2D &v) {
		return Vec2D<T>(x + v.x, y + v.y);
	}
	Vec2D operator-(Vec2D &v) {
		return Vec2D<T>(x - v.x, y - v.y);
	}
	Vec2D operator*(T i) {
		return Vec2D<T>(x * i, y * i);
	}
	Vec2D operator/(T i) {
		return Vec2D<T>(x / i, y / i);
	}

	Vec2D& operator+=(const Vec2D &v) {
		x += v.x;
		y += v.y;
		return *this;
	}
	Vec2D& operator-=(const Vec2D &v) {
		x -= v.x;
		y -= v.y;
		return *this;
	}
	Vec2D& operator*=(T i) {
		x *= i;
		y *= i;
		return *this;
	}
	Vec2D& operator/=(T i) {
		x /= i;
		y /= i;
		return *this;
	}

	inline bool operator==(Vec2D &v) {
		return (x == v.x) && (y == v.y);
	}
	inline bool operator!=(Vec2D &v) {
		return !(this == v);
	}

	inline double lenght() {
		return sqrt(x * x + y * y);
	}

	inline double lenghtSquared() {
		return static_cast<double>(x * x + y * y);
	}

	inline double getAngle() {
		return atan2(x, -y) * 180.0 / M_PI;
	}

	void rotate(double deg) {
		double alfa = deg / 180.0 * M_PI;
		double c = cos(alfa);
		double s = sin(alfa);
		double new_x = x * c - y * s;
		double new_y = x * s + y * c;
		x = new_x;
		y = new_y;
	}

	operator int() {
		x = static_cast<int>(x);
		y = static_cast<int>(y);
		return *this;
	}
	operator float() {
		x = static_cast<float>(x);
		y = static_cast<float>(y);
		return *this;
	}
	operator double() {
		x = static_cast<double>(x);
		y = static_cast<double>(y);
		return *this;
	}
};

// Overload Point operators that use Vec2D

template<class T>
inline SDL_Point operator+(SDL_Point const &p, Vec2D<T> const &v) {
	return SDL_Point { p.x + static_cast<int>(v.x), p.y + static_cast<int>(v.y) };
}

template<class T>
inline SDL_Point operator-(SDL_Point const &p, Vec2D<T> const &v) {
	return SDL_Point { p.x - static_cast<int>(v.x), p.y - static_cast<int>(v.y) };
}

template<class T>
SDL_Point& operator+=(SDL_Point &p, Vec2D<T> const &v) {
	p.x += static_cast<int>(v.x);
	p.y += static_cast<int>(v.y);
	return p;
}

template<class T>
SDL_Point& operator-=(SDL_Point &p, Vec2D<T> const &v) {
	p.x -= static_cast<int>(v.x);
	p.y -= static_cast<int>(v.y);
	return p;
}

template<class T>
inline Point operator+(Point const &p, Vec2D<T> const &v) {
	return Point { p.x + v.x, p.y + v.y };
}

template<class T>
inline Point operator-(Point const &p, Vec2D<T> const &v) {
	return Point { p.x - v.x, p.y - v.y };
}

template<class T>
inline Point& operator+=(Point &p, Vec2D<T> const &v) {
	p.x += v.x;
	p.y += v.y;
	return p;
}

template<class T>
inline Point& operator-=(Point &p, Vec2D<T> const &v) {
	p.x -= v.x;
	p.y -= v.y;
	return p;
}

inline bool vectInRect(SDL_Point &a, SDL_Point &b, SDL_Rect &rect) {
	// Take 2 points A and B and rectangle rect
	// Return True if at least one is on left side of line between A and B, and at least one isn't
	bool is_on_left[4] = { false, false, false, false };
	is_on_left[0] =
			((b.x - a.x) * (rect.y - a.y) - (b.y - a.y) * (rect.x - a.x)) > 0;
	is_on_left[1] = ((b.x - a.x) * (rect.y - a.y)
			- (b.y - a.y) * (rect.x + rect.w - a.x)) > 0;
	is_on_left[2] = ((b.x - a.x) * (rect.y + rect.h - a.y)
			- (b.y - a.y) * (rect.x + rect.w - a.x)) > 0;
	is_on_left[3] = ((b.x - a.x) * (rect.y + rect.h - a.y)
			- (b.y - a.y) * (rect.x - a.x)) > 0;
	return (is_on_left[0] || is_on_left[1] || is_on_left[2] || is_on_left[3])
			&& !(is_on_left[0] && is_on_left[1] && is_on_left[2]
					&& is_on_left[3]);
}
