#include "moving_entity.h"

bool MovingEntity::isAlive() {
	return m_is_alive;
}

Point MovingEntity::getPosition() {
	return m_position;
}

Vec2D<double> & MovingEntity::getVelocity() {
	return m_velocity;
}
