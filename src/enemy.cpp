#include "enemy.h"

#define EPSILON 0.001

Enemy::Enemy(double x, double y, const std::string &path, TextureManager* tm,
		PlayStateMap* map, int index_in_atlas, int tex_size, int textures_num,
		double v, int hp, int hb_x, int hb_y, int hb_w, int hb_h) {
	m_hitbox = new SDL_Rect { hb_x, hb_y, hb_w, hb_h };
	m_hp = hp;
	m_is_alive = true;
	m_texture_manager = tm;
	m_position.x = x;
	m_position.y = y;
	m_sprite = tm->get(path);
	for (int i = 0; i < textures_num; ++i) {
		SDL_Rect* clip = new SDL_Rect;
		clip->x = ((index_in_atlas + i) % (m_sprite->getWidth() / tex_size))
				* tex_size;
		clip->y = ((index_in_atlas + i) / (m_sprite->getHeight() / tex_size))
				* tex_size;
		clip->h = tex_size;
		clip->w = tex_size;
		m_clips.push_back(clip);
	}
	m_clip = m_clips[0];

	// TODO: Instead of looking for available directions on spawn - add 4 variants of spawn paired with direction for spawned enemies
	// Set direction
	int x_loc = static_cast<int>(x - map->getPosition().x) / m_clip->w;
	int y_loc = static_cast<int>(y - map->getPosition().y) / m_clip->h;

	uint8_t available_directions = map->getAvailableDirections(x_loc, y_loc);

	if (y_loc < 200 + tex_size) { // First row of tiles
		m_direction = SOUTH;
	} else if (x_loc < 200 + tex_size) { // First column of tiles
		m_direction = EAST;
	} else {
		m_direction = NORTH;
	}

	// Check if direction is allowed
	if (!(available_directions & m_direction)) {
		uint8_t left = m_direction << 1; // Current direction + 90 deg
		if (left & 1 << 4) {
			left = 1;
		}
		uint8_t right = m_direction >> 1; // Current direction - 90 deg
		if (right == 0) {
			right = 1 << 3;
		}

		if (available_directions & left) { // Turn left
			m_direction = left;
		} else if (available_directions & right) { // Turn right
			m_direction = right;
		}
	}

	// Set angle and velocity based on chosen direction
	int dir_cnt = -1;
	uint8_t dir = m_direction;
	while (dir) {
		dir >>= 1;
		++dir_cnt;
	}

	m_angle = static_cast<double>(dir_cnt) * -90.0;
	Vec2D<double> init_vel = { x = 0.0, y = -v };
	init_vel.rotate(m_angle);
	m_velocity = init_vel;
}

SDL_Rect* Enemy::getHitbox() {
	return m_hitbox;
}

void Enemy::damage(int dmg) {
	m_hp -= dmg;
	if (m_hp <= 0) {
		m_is_alive = false;
	}
}

void Enemy::render() {
	for (unsigned int i = 0; i < m_clips.size() - 1; ++i) {
		m_sprite->renderWithShadow(1, 128, static_cast<int>(m_position.x),
				static_cast<int>(m_position.y), m_clips[i], m_angle);
	}
	m_sprite->renderWithShadow(2, 128, static_cast<int>(m_position.x),
			static_cast<int>(m_position.y), m_clips[m_clips.size() - 1],
			m_angle);
}

void Enemy::update(const double dt, PlayStateMap* map) {

	// Check if entering another tile
	if ((static_cast<int>(m_position.x) / m_clip->w
			!= static_cast<int>(m_position.x + m_velocity.x * dt) / m_clip->w)
			|| (static_cast<int>(m_position.y) / m_clip->h
					!= static_cast<int>(m_position.y + m_velocity.y * dt)
							/ m_clip->h)) {

		// Get available directions
		// If v.x or v.y < 0, check right bottom corner
		int x = static_cast<int>(m_position.x - map->getPosition().x
				+ m_velocity.x * dt) / m_clip->w
				+ static_cast<int>(m_velocity.x > EPSILON
						|| m_velocity.x < -EPSILON);
		int y = static_cast<int>(m_position.y - map->getPosition().y
				+ m_velocity.y * dt) / m_clip->h
				+ static_cast<int>(m_velocity.y > EPSILON
						|| m_velocity.y < -EPSILON);

		uint8_t available_directions = map->getAvailableDirections(x, y);

		// Move
		m_position += m_velocity * dt;

		// If m_direction NOT in available directions
		if (!(available_directions & m_direction)) {

			// Check if changing direction is possible
			uint8_t left = m_direction << 1; // Current direction + 90 deg
			if (left & 1 << 4) {
				left = 1;
			}
			uint8_t right = m_direction >> 1; // Current direction - 90 deg
			if (right == 0) {
				right = 1 << 3;
			}

			if (available_directions & left) { // Turn left
				m_angle -= 90.0;
				m_direction = left;
				m_position.x = static_cast<double>(map->getPosition().x)
						+ x * m_clip->w;
				m_position.y = static_cast<double>(map->getPosition().y)
						+ y * m_clip->h;
				m_velocity.rotate(-90.0);
			} else if (available_directions & right) { // Turn right
				m_angle += 90.0;
				m_direction = right;
				m_velocity.rotate(90.0);
				m_position.x = static_cast<double>(map->getPosition().x)
						+ x * m_clip->w;
				m_position.y = static_cast<double>(map->getPosition().y)
						+ y * m_clip->h;
			} else { // Stop
				m_velocity.x = 0.0;
				m_velocity.y = 0.0;
				m_direction = 0;
			}

			// If component is close to 0, set it 0.0
			if (m_velocity.x < EPSILON && m_velocity.x > -EPSILON) {
				m_velocity.x = 0.0;
			}
			if (m_velocity.y < EPSILON && m_velocity.y > -EPSILON) {
				m_velocity.y = 0.0;
			}
		}
	} else {
		// Move
		m_position += (m_velocity * dt);
	}

	// Check if reached end of path
	if (m_position.x < map->getPosition().x
			|| m_position.x
					> (map->getPosition().x
							+ (map->getVectorSizeX() - 1) * m_clip->w)
			|| m_position.y < map->getPosition().y
			|| m_position.y
					> (map->getPosition().y
							+ (map->getVectorSizeY() - 1) * m_clip->h)) {
		// Decrease player lives
		m_is_alive = false;
	}
}

Enemy::~Enemy() {
	for (auto &clip : m_clips) {
		delete clip;
	}
	m_clips.clear();
	m_clip = nullptr;
	delete m_hitbox;
	m_hitbox = nullptr;
}
