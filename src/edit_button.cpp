#include "edit_button.h"

#include "game.h"

void EditButton::onClick() {
	m_game->pushState(new EditState(m_game));
}
