#include "save_map_button.h"

#include "map.h"

SaveMapButton::SaveMapButton(int x, int y, const std::string &path, Game* game,
		Map* map) :
		Button(x, y, path, game) {
	m_map = map;
}

void SaveMapButton::onClick() {
	m_map->saveMap(m_map->getMapPath());
}
