#pragma once

#include "button.h"
#include "map.h"

class LoadMapButton: public Button {
public:
	LoadMapButton(int x, int y, const std::string &path, Game* game, Map* map);
	LoadMapButton(const LoadMapButton &) = delete;
	LoadMapButton& operator=(const LoadMapButton &) = delete;

	void onClick();
private:
	Map* m_map;
};
