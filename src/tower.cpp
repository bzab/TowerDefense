#include "tower.h"

#include <cstdlib>

#include "play_state_map.h"

Tower::Tower(int x, int y, const std::string &path, TextureManager* tm,
		int index_in_atlas, int tex_size, int b_t_num, int textures_num,
		int dmg, double range, double bul_spd, double rof) {
	m_angle = -90.0;
	m_base_textures_num = b_t_num;
	m_bullet_spd = bul_spd;
	m_cooldown_timer = 0.0;
	m_dmg = dmg;
	m_id = index_in_atlas;
	m_is_active = false;
	m_is_clicked = false;
	m_is_hovered = false;
	m_texture_manager = tm;
	m_range = range;
	m_rof = rof;
	m_position.x = x;
	m_position.y = y;
	m_sprite = tm->get(path);

	for (int i = 0; i < textures_num; ++i) {
		SDL_Rect* clip = new SDL_Rect;
		clip->x = ((index_in_atlas + i) % (m_sprite->getWidth() / tex_size))
				* tex_size;
		clip->y = ((index_in_atlas + i) / (m_sprite->getHeight() / tex_size))
				* tex_size;
		clip->h = tex_size;
		clip->w = tex_size;
		m_clips.push_back(clip);
	}
	m_clip = m_clips[0];
}

Tower::Tower(int x, int y, const Tower &t) {
	m_angle = t.m_angle;
	m_base_textures_num = t.m_base_textures_num;
	m_bullet_spd = t.m_bullet_spd;
	m_cooldown_timer = 0.0;
	m_dmg = t.m_dmg;
	m_id = t.m_id;
	m_is_active = false;
	m_is_clicked = false;
	m_is_hovered = false;
	m_texture_manager = t.m_texture_manager;
	m_range = t.m_range;
	m_rof = t.m_rof;
	m_position.x = x;
	m_position.y = y;
	m_sprite = t.m_sprite;

	for (auto &t_clip : t.m_clips) {
		SDL_Rect* clip = new SDL_Rect { t_clip->x, t_clip->y, t_clip->h,
				t_clip->w };
		m_clips.push_back(clip);
	}
	m_clip = m_clips[0];
}

bool Tower::isActive() {
	return m_is_active;
}

void Tower::flipActive() {
	m_is_active = !m_is_active;
}

int Tower::getHeight() {
	return m_clips[0]->h;
}

int Tower::getWidth() {
	return m_clips[0]->w;
}

int Tower::getTowerId() {
	return m_id;
}

double Tower::getRange() {
	return m_range;
}

void Tower::onClick() {
	flipActive();
}

void Tower::render() {
	// TODO: Render background

	for (unsigned int i = 0; i < m_clips.size(); ++i) {
		if (m_is_hovered) {
			m_sprite->setColor(200, 200, 200);
		} else {
			m_sprite->setColor(255, 255, 255);
		}
		if (i < m_base_textures_num) {
			m_sprite->renderWithShadow(1, 128, m_position.x, m_position.y,
					m_clips[i], -90.0);
		} else {
			m_sprite->renderWithShadow(1, 64, m_position.x, m_position.y,
					m_clips[i], m_angle);
		}
	}
	if (m_is_active) {
		SDL_Rect active_clip = { 0, 0, m_clips[0]->w, m_clips[0]->h };
		m_sprite->render(m_position.x, m_position.y, &active_clip);
	}
}

void Tower::rotate(double angle) {
	m_angle = angle;
}

void Tower::setActive(bool f) {
	m_is_active = f;
}

bool Tower::update(const double dt, Vec2D<double> &aim,
		Vec2D<double> target_vel, PlayStateMap* map) {
	// Calculate adjustment
	aim += target_vel * dt * (aim.lenght() / m_bullet_spd);

	m_angle = aim.getAngle();
	if (m_cooldown_timer > m_rof) {
		if (aim.lenghtSquared() < m_range * m_range) {
			map->spawnBullet(static_cast<double>(m_position.x + m_clip->w / 2),
					static_cast<double>(m_position.y + m_clip->h / 2), m_angle,
					m_range, m_bullet_spd, m_dmg);
			m_cooldown_timer = 0.0;
			return true; // Break loop in play state
		}
		return false; // Ready to fire, target not in range -> dont break loop
	} else {
		return true;
	}
}

void Tower::updateTimer(const double dt) {
	if (m_cooldown_timer < m_rof * 2) {
		m_cooldown_timer += dt;
	}
}

Tower::~Tower() {
	for (auto &clip : m_clips) {
		delete clip;
	}
	m_clips.clear();
	m_clip = nullptr;
}
