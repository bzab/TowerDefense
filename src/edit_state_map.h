#pragma once

#include "map.h"
#include "texture.h"

class EditStateMap: public Map {
public:
	EditStateMap(int x, int y, int tile_size, const std::string &path,
			const std::string &atlas_path, Game* game);
	EditStateMap(const EditStateMap &) = delete;
	EditStateMap& operator=(const EditStateMap &) = delete;

	void handleEvent(SDL_Event* e);
	void render();
	void saveMap(const std::string &path);
	void setSwap(int tile_id);
	void swapTile(int tile_id); //Arg: TileType ID

	~EditStateMap();

private:
	int m_idTileSwap; // 0 = don't swap
	int m_idTurret;   // 0 = don't put

	SDL_Rect* m_spawn_clip;
	Texture* m_spawn_sprite;

};
