#pragma once

#include "button.h"
#include "map.h"

class SaveMapButton: public Button {
public:
	SaveMapButton(int x, int y, const std::string &path, Game* game, Map* map);
	SaveMapButton(const SaveMapButton &) = delete;
	SaveMapButton& operator=(const SaveMapButton &) = delete;

	void onClick();
private:
	Map* m_map;
};
