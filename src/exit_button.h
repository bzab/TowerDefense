#pragma once

#include "button.h"

class ExitButton: public Button {
public:
	using Button::Button;

	void onClick();
};
