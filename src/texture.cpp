#include "texture.h"

Texture::Texture(const std::string &path, SDL_Renderer* renderer) {

	m_renderer = renderer;
	m_texture = NULL;
	m_width = 0;
	m_height = 0;

	if (!loadFromFile(path)) {
		std::cerr << "Texture " << path << " initialization failed"
				<< std::endl;
	}
}

Texture::~Texture() {
	free();
}

bool Texture::loadFromFile(const std::string &path) {
	free();

	SDL_Texture* new_texture = NULL;

	SDL_Surface* loaded_surface = IMG_Load(path.c_str());
	if (loaded_surface == NULL) {
		std::cerr << "Unable to load image " << path.c_str()
				<< "! SDL_image Error: " << IMG_GetError() << std::endl;
	} else {
		//Color key image
		SDL_SetColorKey(loaded_surface, SDL_TRUE,
				SDL_MapRGB(loaded_surface->format, 0, 0xFF, 0xFF));

		new_texture = SDL_CreateTextureFromSurface(m_renderer, loaded_surface);
		if (new_texture == NULL) {
			std::cerr << "Unable to create texture from " << path.c_str()
					<< "! SDL Error: " << SDL_GetError() << std::endl;
		} else {
			m_width = loaded_surface->w;
			m_height = loaded_surface->h;
		}

		SDL_FreeSurface(loaded_surface);
	}

	//Return success
	m_texture = new_texture;
	return m_texture != NULL;
}

#ifdef _SDL_TTF_H
bool Texture::loadFromRenderedText( std::string textureText, SDL_Color textColor )
{
	free();

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid( gFont, textureText.c_str(), textColor );
	if( textSurface != NULL )
	{
		m_texture = SDL_CreateTextureFromSurface( m_Rrenderer, textSurface );
		if( m_texture == NULL )
		{
			std::cerr << "Unable to create texture from rendered text! SDL Error: " << SDL_GetError() << std::endl;
		}
		else
		{
			m_width = textSurface->w;
			m_height = textSurface->h;
		}

		SDL_FreeSurface( textSurface );
	}
	else
	{
		std::cerr << "Unable to render text surface! SDL_ttf Error: " << TTF_GetError() << std::endl;
	}

	//Return success
	return m_texture != NULL;
}
#endif

void Texture::free() {
	if (m_texture != NULL) {
		SDL_DestroyTexture(m_texture);
		m_texture = NULL;
		m_width = 0;
		m_height = 0;
	}
}

void Texture::setColor(Uint8 red, Uint8 green, Uint8 blue) {
	//Modulate texture rgb
	SDL_SetTextureColorMod(m_texture, red, green, blue);
}

void Texture::setBlendMode(SDL_BlendMode blending) {
	//Set blending function
	SDL_SetTextureBlendMode(m_texture, blending);
}

void Texture::setAlpha(Uint8 alpha) {
	//Modulate texture alpha
	SDL_SetTextureAlphaMod(m_texture, alpha);
}

void Texture::render(int x, int y, SDL_Rect* clip, double angle,
		SDL_Point* center, SDL_RendererFlip flip) {
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, m_width, m_height };

	//Set clip rendering dimensions
	if (clip != NULL) {
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	//Render to screen
	SDL_RenderCopyEx(m_renderer, m_texture, clip, &renderQuad, angle, center,
			flip);
}

void Texture::renderWithShadow(Uint8 shadow_size, Uint8 alpha, int x, int y,
		SDL_Rect* clip, double angle, SDL_Point* center,
		SDL_RendererFlip flip) {
	setColor(0, 0, 0);
	setAlpha(alpha);
	render(x + 1, y + 1, clip, angle);
	setColor(255, 255, 255);
	setAlpha(255);
	render(x, y, clip, angle);
}

int Texture::getWidth() {
	return m_width;
}

int Texture::getHeight() {
	return m_height;
}
