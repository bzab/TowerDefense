#include "game.h"
#include "main_menu_state.h"

int main() {
	//       Width, Height
	Game game(1920, 1080);

	// Enter main menu on start
	game.pushState(new MainMenuState(&game));

	// Start loop
	game.gameLoop();

	return 0;
}
