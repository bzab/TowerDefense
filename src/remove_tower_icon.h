#pragma once

#include "clickable_gui_object.h"

class RemoveTowerIcon: public ClickableGUIObject {
public:
	RemoveTowerIcon(int x, int y, const std::string &path, TextureManager* tm,
			int index_in_atlas, int tex_size);
	RemoveTowerIcon(const RemoveTowerIcon &) = delete;
	RemoveTowerIcon& operator=(const RemoveTowerIcon &) = delete;

	bool isActive();

	void flipActive();
	void setActive(bool f);
	void onClick();
	void render();

private:
	bool m_is_active;
};
