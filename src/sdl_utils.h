#pragma once

#include <cmath>

#include "geo_2d.h"
#include "sdl_includes.h"

inline bool operator==(SDL_Point const &p1, SDL_Point const &p2) {
	return (p1.x == p2.x) && (p1.y == p2.y);
}

inline bool operator!=(SDL_Point const &p1, SDL_Point const &p2) {
	return !(p1 == p2);
}

inline double distance(SDL_Point const &p1, SDL_Point const &p2) {
	return sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}
